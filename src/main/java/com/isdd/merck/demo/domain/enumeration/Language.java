package com.isdd.merck.demo.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH, ENGLISH, SPANISH
}
