/**
 * Data Access Objects used by WebSocket services.
 */
package com.isdd.merck.demo.web.websocket.dto;
