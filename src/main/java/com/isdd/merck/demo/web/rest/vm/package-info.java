/**
 * View Models used by Spring MVC REST controllers.
 */
package com.isdd.merck.demo.web.rest.vm;
