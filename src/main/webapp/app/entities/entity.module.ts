import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JhipsterMerckDemoRegionMySuffixModule } from './region-my-suffix/region-my-suffix.module';
import { JhipsterMerckDemoCountryMySuffixModule } from './country-my-suffix/country-my-suffix.module';
import { JhipsterMerckDemoLocationMySuffixModule } from './location-my-suffix/location-my-suffix.module';
import { JhipsterMerckDemoDepartmentMySuffixModule } from './department-my-suffix/department-my-suffix.module';
import { JhipsterMerckDemoTaskMySuffixModule } from './task-my-suffix/task-my-suffix.module';
import { JhipsterMerckDemoEmployeeMySuffixModule } from './employee-my-suffix/employee-my-suffix.module';
import { JhipsterMerckDemoJobMySuffixModule } from './job-my-suffix/job-my-suffix.module';
import { JhipsterMerckDemoJobHistoryMySuffixModule } from './job-history-my-suffix/job-history-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        JhipsterMerckDemoRegionMySuffixModule,
        JhipsterMerckDemoCountryMySuffixModule,
        JhipsterMerckDemoLocationMySuffixModule,
        JhipsterMerckDemoDepartmentMySuffixModule,
        JhipsterMerckDemoTaskMySuffixModule,
        JhipsterMerckDemoEmployeeMySuffixModule,
        JhipsterMerckDemoJobMySuffixModule,
        JhipsterMerckDemoJobHistoryMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterMerckDemoEntityModule {}
