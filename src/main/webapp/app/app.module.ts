import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ngx-webstorage';

import { JhipsterMerckDemoSharedModule, UserRouteAccessService } from './shared';
import { JhipsterMerckDemoAppRoutingModule} from './app-routing.module';
import { JhipsterMerckDemoHomeModule } from './home/home.module';
import { JhipsterMerckDemoAdminModule } from './admin/admin.module';
import { JhipsterMerckDemoAccountModule } from './account/account.module';
import { JhipsterMerckDemoEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        JhipsterMerckDemoAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        JhipsterMerckDemoSharedModule,
        JhipsterMerckDemoHomeModule,
        JhipsterMerckDemoAdminModule,
        JhipsterMerckDemoAccountModule,
        JhipsterMerckDemoEntityModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class JhipsterMerckDemoAppModule {}
